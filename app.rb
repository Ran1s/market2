#require './models/models.rb'
require './models/client.rb'
require './models/order.rb'
require './models/shop.rb'
require './models/product.rb'

shop1 = Shop.new({name: "Shop1", city: "Kazan"})
shop2 = Shop.new({name: "Shop2", city: "Moscow"})
shop3 = Shop.new({name: "Shop3", city: "Kazan"})
shop4 = Shop.new({name: "Shop4", city: "Kazan"})

shop1.save; shop2.save; shop3.save; shop4.save

shop1.add_product({name: "TShirt", price: 100, amount: 50}).save
shop2.add_product({name: "Cool TShirt", price: 200, amount: 50}).save
shop3.add_product({name: "TShirt", price: 150, amount: 10}).save
shop4.add_product({name: "iPhone 6", price: 50000, amount: 50}).save


shops = Shop.where
shops_in_kazan = Shop.where { |shop| shop[:city] == "Kazan" }
p shops_in_kazan

products = Product.where
tshirts = Product.where { |product| product[:name] == "TShirt" }

shop3.name = "The Best Shop"
shop3.save


client = Client.new({name: "John Smith", city: "Kazan"})
client.save
client.add_balance(1000)


product = tshirts[0]

client.add_to_order(product).save
client.purchase
client.add_to_order(tshirts[0])
client.purchase
p client
p client.get_orders
client.purchase
p client
