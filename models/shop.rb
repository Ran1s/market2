require './models/base_model.rb'

class Shop < BaseModel

  attr_accessor :name, :city

  def add_product(product_info)
    product_info[:id_shop] = self.id
    Product.new(product_info)
  end

  def get_products
    Product.where { |product| product[:id_shop] == self.id }
  end

end
