
class CollectionProxy

  def initialize(objects, &block)
    self.objects = objects
    conditions << block
  end

  def where(&block)
    conditions << block
    self
  end

  def to_a
    return objects if conditions.empty?

    objects_temp = objects

    conditions.each do |condition|
      objects_temp.select!(&condition)
    end

    objects_temp
  end

  def each
    self.to_a.each { |object| yield object }
  end

  def delete_all
    self.each { |object| object.delete }
    self
  end

  private
  def conditions
    @conditions ||= []
  end

  def conditions=(value)
    @conditions = value
  end

  def objects
    @objects ||= []
  end

  def objects=(value)
    @objects = value
  end

end
