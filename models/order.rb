require './models/base_model.rb'
class Order < BaseModel
  attr_accessor :paid
  attr_reader :id_client, :id_product

  def initialize(order_info)
    super
    self.paid = false if order_info[:paid].nil?
  end

end
