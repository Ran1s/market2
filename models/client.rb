require './models/base_model.rb'
class Client < BaseModel

  attr_accessor :name, :city
  attr_reader :balance

  def initialize(client_info)
    super
    @balance = 0 if client_info[:balance].nil?
  end

  def add_balance(amount)
    @balance += amount
  end

  def get_orders
    Order.where { |order| order[:id_client] == self.id }
  end

  def purchase
    sum = 0
    orders = get_orders
    orders.each do |order|
      next if order.paid == true
      product = Product.get(order.id_product)
      sum += product.price
    end
    if sum <= self.balance
      orders.each do |order|
        order.paid = true
        order.save
      end
      @balance -= sum
      return true
    else
      return false
    end
  end


  def add_to_order(product)
    return false if Product.get(product.id).amount < 1

    order_info = {
      id_product: product.id,
      id_client: self.id
    }
    Order.new(order_info)
  end

end
