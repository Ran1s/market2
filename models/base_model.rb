require './models/storage.rb'
require './models/collection_proxy.rb'
class BaseModel
  attr :id

  def self.new(data)
    return nil if data.nil?
    super
  end

  def initialize(data)
    @id = nil

    data.each do |property, value|
      self.instance_variable_set("@#{property}".to_sym, value)
    end

  end

  def save
    new_data = objects_storage.save(to_data)
    @id = new_data[:id]
    self
  end

  def delete
    objects_storage.delete(id)
  end


  class << self
    def delete_all
      objects_storage.delete_all
    end

    def get(id)
      new(objects_storage.where { |object_data| object_data[:id] == id }.first)
    end

    def where(&block)
      CollectionProxy.new(objects_storage.where.map { |object_data| new(object_data) }, &block)
      #objects_storage.where(&block).map { |object_data| new(object_data) }
    end
  end


  # def method_missing(m, *args)
  #
  # end


  private
  def objects_storage; self.class.objects_storage; end

  def self.objects_storage
    @objects_storage ||= Storage.new
  end

  def to_data
    Hash[self.instance_variables.map { |variable| [variable.to_s[1..-1].to_sym, self.instance_variable_get(variable)] }]
  end

end
