class Storage

  def save(data)
    if data[:id].nil?
      data[:id] = get_next_id
      objects << data
    else
      update(data)
    end
    data
  end

  def where(&block)
    return objects if block.nil?

    objects.select(&block)
  end

  def delete(id)
    objects.delete_if { |object| object[:id] == id }
    where { |object| object[:id] == id }.first
  end

  def delete_all
    objects.clear
    @next_id = 0
    []
  end

  private  
  def update(data)
    index = objects.index { |object| object[:id] == data[:id] }
    objects[index] = data
  end

  def objects
    @objects ||= []
  end

  def objects=(value)
    @objects = value
  end

  def get_next_id
    @next_id = (@next_id.nil?) ? 1 : @next_id + 1
  end

end
