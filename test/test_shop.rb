require './models/shop.rb'
require './models/product.rb'
require 'minitest/autorun'

class TestShop < MiniTest::Test

  def setup
    Shop.delete_all
  end

  def to_data(object)
    Hash[object.instance_variables.map { |variable| [variable.to_s[1..-1].to_sym, object.instance_variable_get(variable)] }]
  end

  def test_initialize_shop
    shop1 = Shop.new({name: 'Shop1', city: 'Kazan'}).save
    shop_response = Shop.get(1)
    assert_equal(to_data(shop1), to_data(shop_response))
  end

  def test_get_by_id
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop_response = Shop.get(1)
    assert_equal(to_data(shop), to_data(shop_response))
  end

  def test_get_by_id_nil
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    assert_equal(nil, Shop.get(2))
  end

  def test_get_by_city
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'magaz2', city: 'Kazan'}).save
    Shop.new({name: 'magaz2', city: 'Moscow'}).save
    shops = Shop.where { |shop| shop[:city] == 'Kazan' }.map { |shop| to_data(shop) }
    assert_equal([to_data(shop), to_data(shop2)], shops)
  end

  def test_delete_all
    Shop.new({name: 'magaz', city: 'Kazan'}).save
    Shop.new({name: 'magaz2', city: 'Kazan'}).save
    Shop.new({name: 'magaz2', city: 'Moscow'}).save
    Shop.delete_all

    assert_equal([], Shop.where)
  end

  def test_get_all
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'magaz2', city: 'Kazan'}).save
    shop3 = Shop.new({name: 'magaz2', city: 'Moscow'}).save
    shops = Shop.where.map { |shop| to_data(shop) }
    assert_equal([to_data(shop), to_data(shop2), to_data(shop3)], shops)
  end

  def test_delete
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'magaz2', city: 'Kazan'}).save
    shop3 = Shop.new({name: 'magaz2', city: 'Moscow'}).save
    shop2.delete
    shops = Shop.where.map { |shop| to_data(shop) }
    assert_equal([to_data(shop), to_data(shop3)], shops)
  end

  #def test_to_data
  #  Shop.delete_all
  #  shop = Shop.new({name: 'magaz', city: 'Kazan'})
  #  assert_equal({id: 1, name: 'magaz', city: 'Kazan'}, shop.to_data)
  #end

  #def test_add_product
  #  Shop.delete_all
  #  Product.delete_all
  #  shop = Shop.new({name: 'magaz', city: 'Kazan'})
  #  shop2 = Shop.new({name: 'magaz2', city: 'Kazan'})
  #  shop.add_product({name: "T­Shirt", price: 100, amount: 50})
  #  shop2.add_product(  {name: "Cool T­Shirt", price: 100, amount: 50})
  #  response = Product.find(:id_shop, shop.id)
  #  products = []
  #  response.each do |product|
  #    products << {id: product.id, name: product.price, city: product.amount}
  #  end
  #  assert_equal([{id: 1, name: "T­Shirt", price: 100, amount: 50, id_shop: shop.id}], products)
  #end

  #def test_get_products
  #  Shop.delete_all
  #  Product.delete_all
  #  shop = Shop.new({name: 'magaz', city: 'Kazan'})
  #  shop2 = Shop.new({name: 'magaz2', city: 'Kazan'})
  #  shop.add_product({name: "T­Shirt", price: 100, amount: 50})
  #  shop2.add_product({name: "Cool T­Shirt", price: 100, amount: 50})
  #  response = shop.get_products
  #  products = []
  #  response.each do |product|
  #    products << {id: product.id, name: product.price, city: product.amount}
  #  end
  #  assert_equal([{id: 1, name: "T­Shirt", price: 100, amount: 50, id_shop: shop.id}], products)
  #end



end
