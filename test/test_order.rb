require './models/client.rb'
require './models/order.rb'
require './models/shop.rb'
require './models/product.rb'
require 'minitest/autorun'

class TestOrder < MiniTest::Test

  def setup
    Client.delete_all
    Order.delete_all
    Shop.delete_all
    Product.delete_all
  end

  def to_data(object)
    Hash[object.instance_variables.map { |variable| [variable.to_s[1..-1].to_sym, object.instance_variable_get(variable)] }]
  end

  def test_initialize_order
    order = Order.new({id_client: 1, id_product: 1}).save
    assert_equal(to_data(order), to_data(Order.get(1)))
  end

  def test_get_by_id
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save

    order = client.add_to_order(product).save
    assert_equal(to_data(order), to_data(Order.get(1)))
  end

  def test_get_by_id_nil
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save

    order = client.add_to_order(product).save
    assert_equal(nil, Order.get(2))
  end

  def test_find_by_client
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'shop', city: 'Kazan'}).save
    product = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    product2 = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product3 = shop2.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    product4 = shop2.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    client2 = Client.new({name: 'Petya', city: 'Kazan', balance: 100}).save

    client.add_to_order(product2).save
    client.add_to_order(product3).save

    client2.add_to_order(product).save
    client2.add_to_order(product2).save
    client2.add_to_order(product4).save

    orders = Order.where { |order| order[:id_client] == client.id }.map { |order| to_data(order) }


    assert_equal([{id: 1, id_client: client.id, id_product: product2.id, paid: false}, {id: 2, id_client: client.id, id_product: product3.id, paid: false}], orders)
  end

  def test_delete
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'shop', city: 'Kazan'}).save
    product = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    product2 = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product3 = shop2.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    product4 = shop2.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    client.save

    client.add_to_order(product).save
    order2 = client.add_to_order(product2).save
    client.add_to_order(product3).save
    client.add_to_order(product4).save

    order2.delete

    orders = Order.where.map { |order| to_data(order) }

    assert_equal([{id: 1, id_client: client.id, id_product: product.id, paid: false}, {id: 3, id_client: client.id, id_product: product3.id, paid: false}, {id: 4, id_client: client.id, id_product: product4.id, paid: false}, ], orders)
  end

  def test_delete_all
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'shop', city: 'Kazan'}).save
    product = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    product2 = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product3 = shop2.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    product4 = shop2.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    client.save


    client.add_to_order(product).save
    client.add_to_order(product2).save
    client.add_to_order(product3).save
    client.add_to_order(product4).save

    Order.delete_all


    assert_equal([], Order.where)
  end

  # def test_to_data
  #   Order.delete_all
  #   order = Order.new({id_client: 1, id_product: 1})
  #   assert_equal({id: 1, id_client: 1, id_product: 1, paid: false}, order.to_data)
  # end


end
