require './models/product.rb'
require './models/shop.rb'
require 'minitest/autorun'

class TestProduct < MiniTest::Test

  def setup
    Shop.delete_all
    Product.delete_all
  end

  def to_data(object)
    Hash[object.instance_variables.map { |variable| [variable.to_s[1..-1].to_sym, object.instance_variable_get(variable)] }]
  end

  def test_initialize_product
    #shop = Shop.new({name: 'Shop1', city: 'Kazan'})
    #product = shop.add_product({name: "T­Shirt", price: 100, amount: 50})
    product = Product.new({name: "T­Shirt", price: 100, amount: 50, id_shop: 1}).save
    product_response = Product.get(1)
    assert_equal(to_data(product), to_data(product_response))
  end

  def test_get_by_id
    shop = Shop.new({name: 'magaz', city: 'Kazan'})
    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product_response = Product.get(1)
    assert_equal(to_data(product), to_data(product_response))
  end

  def test_get_by_id_nil
    shop = Shop.new({name: 'magaz', city: 'Kazan'})
    shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    assert_equal(nil, Product.get(2))
  end

  def test_get_by_name
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'magaz2', city: 'Kazan'}).save

    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    shop.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    product3 = shop2.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    shop2.add_product({name: "The Coolest T­Shirt", price: 100, amount: 50}).save

    products = Product.where { |product| product[:name] == "T­Shirt" }.map { |product| to_data(product) }

    assert_equal([to_data(product), to_data(product3)], products)
  end

  def test_get_by_price
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'magaz2', city: 'Kazan'}).save

    product = shop.add_product({name: "T­Shirt", price: 123, amount: 50}).save
    shop.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    shop2.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product4 = shop2.add_product({name: "The Coolest T­Shirt", price: 123, amount: 50}).save
    products = Product.where { |product| product[:price] == 123 }.map { |product| to_data(product) }

    assert_equal([to_data(product), to_data(product4)], products)
  end

  def test_get_by_amount
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop2 = Shop.new({name: 'magaz2', city: 'Kazan'}).save

    shop.add_product({name: "T­Shirt", price: 123, amount: 50}).save
    product2 = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 321}).save
    product3 = shop2.add_product({name: "T­Shirt", price: 100, amount: 321}).save
    shop2.add_product({name: "The Coolest T­Shirt", price: 123, amount: 50}).save

    products = Product.where { |product| product[:amount] == 321 }.map { |product| to_data(product) }

    assert_equal([to_data(product2), to_data(product3)], products)
  end

  def test_delete_all
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    shop.add_product({name: "T­Shirt", price: 123, amount: 50}).save
    shop.add_product({name: "Cool T­Shirt", price: 100, amount: 321}).save
    shop.add_product({name: "T­Shirt", price: 100, amount: 321}).save
    shop.add_product({name: "The Coolest T­Shirt", price: 123, amount: 50}).save

    Product.delete_all

    assert_equal([], Product.where)
  end

  def test_where
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 123, amount: 50}).save
    product2 = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 321}).save
    product3 = shop.add_product({name: "T­Shirt", price: 100, amount: 321}).save
    product4 = shop.add_product({name: "The Coolest T­Shirt", price: 123, amount: 50}).save

    products = Product.where.map { |product| to_data(product) }

    assert_equal([to_data(product), to_data(product2), to_data(product3), to_data(product4)], products)
  end

  def test_delete
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 123, amount: 50}).save
    product2 = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 321}).save
    product3 = shop.add_product({name: "T­Shirt", price: 100, amount: 321}).save
    product4 = shop.add_product({name: "The Coolest T­Shirt", price: 123, amount: 50}).save
    product2.delete
    products = Product.where.map { |product| to_data(product) }

    assert_equal([to_data(product), to_data(product3), to_data(product4)], products)
  end

  #def test_to_data
  #  Shop.delete_all
  #  Product.delete_all
  #  shop = Shop.new({name: 'magaz', city: 'Kazan'})
  #  product = shop.add_product({name: "T­Shirt", price: 123, amount: 50})
  #  assert_equal({id: 1, name: "T­Shirt", price: 123, amount: 50, id_shop: 1}, product.to_data)
  #end

end
