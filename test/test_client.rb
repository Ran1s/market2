require './models/client.rb'
require './models/order.rb'
require './models/shop.rb'
require './models/product.rb'
require 'minitest/autorun'

class TestClient < MiniTest::Test

  def setup
    Client.delete_all
    Order.delete_all
    Shop.delete_all
    Product.delete_all
  end

  def to_data(object)
    Hash[object.instance_variables.map { |variable| [variable.to_s[1..-1].to_sym, object.instance_variable_get(variable)] }]
  end

  def test_initialize_client
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    assert_equal(to_data(client), to_data(Client.get(1)))
  end

  def test_get_by_id
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    assert_equal(to_data(client), to_data(Client.get(1)))
  end

  def test_get_by_id_nil
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    assert_equal(nil, Client.get(2))
  end

  def test_add_balance
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    client.add_balance(100)
    assert_equal(200, client.balance)
  end

  def test_add_to_order
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    client.save

    order = client.add_to_order(product)
    order.save
    assert_equal({id: 1, id_client: client.id, id_product: product.id, paid: false  }, to_data(Order.get(1)))
  end

  def test_get_orders
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product2 = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save

    order = client.add_to_order(product).save
    order2 = client.add_to_order(product2).save

    orders = client.get_orders.map { |order| to_data(order) }

    assert_equal([{id: 1, id_client: client.id, id_product: product.id, paid: false}, {id: 2, id_client: client.id, id_product: product2.id, paid: false}], orders)
  end

  def test_purchase_valid
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product2 = shop.add_product({name: "Cool T­Shirt", price: 100, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 200}).save
    client.add_to_order(product).save
    client.add_to_order(product2).save

    client.purchase

    assert_equal(true, client.purchase)
  end

  def test_purchase_invalid
    shop = Shop.new({name: 'magaz', city: 'Kazan'}).save
    product = shop.add_product({name: "T­Shirt", price: 100, amount: 50}).save
    product2 = shop.add_product({name: "Cool T­Shirt", price: 101, amount: 50}).save
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 200}).save
    client.add_to_order(product).save
    client.add_to_order(product2).save
    assert_equal(false, client.purchase)
  end

  def test_delete
    client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    client2 = Client.new({name: 'Petya', city: 'Kazan', balance: 200}).save
    client3 = Client.new({name: 'Vanya', city: 'Kazan', balance: 300}).save
    client2.delete

    clients = Client.where.map { |client| to_data(client) }

    assert_equal([to_data(client), to_data(client3)], clients)
  end

  def test_delete_all
    Client.new({name: 'Vasya', city: 'Kazan', balance: 100}).save
    Client.new({name: 'Petya', city: 'Kazan', balance: 200}).save
    Client.new({name: 'Vanya', city: 'Kazan', balance: 300}).save
    Client.delete_all

    assert_equal([], Client.where)
  end

  # def test_to_data
  #   Client.delete_all
  #   client = Client.new({name: 'Vasya', city: 'Kazan', balance: 100})
  #
  #   assert_equal({id: 1, name: 'Vasya', city: 'Kazan', balance: 100}, client.to_data)
  #
  # end

end
